FROM seashelltechnology/arm32v6:alpine-edge-xbuild

RUN [ "seashell-xbuild-start" ]
RUN cat /etc/os-release
RUN [ "seashell-xbuild-stop" ]

COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]